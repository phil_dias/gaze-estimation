classdef lineInterpolation < vision.labeler.AutomationAlgorithm & vision.labeler.mixin.Temporal

    
    %----------------------------------------------------------------------
    % Algorithm Description
    %----------------------------------------------------------------------    
    properties(Constant)
        
        %Name Algorithm Name
        %   Character vector specifying name of algorithm.
        Name = 'Custom 2Pt. Line algorithm';
        
        %Description Algorithm Description
        %   Character vector specifying short description of algorithm.
        Description = 'Description';
        
        %UserDirections Algorithm Usage Directions

        UserDirections = {'Directions'};
    end

    %----------------------------------------------------------------------
    % Properties
    %----------------------------------------------------------------------    
    properties
        
        %InitialLabels Set of labels at algorithm initialization
        %   Table with columns Name, Type and Position for labels marked at
        %   initialization.        
        InitialLabels;
        
        %prevROI Previous ROI position       
        prevLine1;   
        prevLine2; 
       
        %delX change in ROI position in x-direction
        delX1;
        
        %delY change in ROI position in y-direction      
        delY1;
        
        %delX2 change in ROI width       
        delX2;
        
        %delY2 change in ROI height       
        delY2;  
        
        %ROIType ROI type
        %   A character array
        ROIType;
        
        %ROIName ROI name
        %   A character array
        ROIName;
              
        %Idx Index
        %   A scalar
        Idx;   
        
        counter;
    end
    
    properties (Access = private)
        
        %IsForwardAutomation Checks if automation is in forward direction     
        %   True if automation is in forward direction, False if automation
        %   is in reverse direction.
        IsForwardAutomation = true;        
    end
    
    %----------------------------------------------------------------------
    % Setup
    %----------------------------------------------------------------------
    methods
        
        function flag = supportsReverseAutomation(~)            
            flag = true;
        end  
        
        function isValid = checkLabelDefinition(~, labelDef)
            
            % Only Rectangular ROI Label definitions are valid for the
            % Temporal Interpolator.
            isValid = labelDef.Type == labelType.Line;
        end
        
        function isReady = checkSetup(~, labelsToAutomate)
            
            % Check-1: There must be at least two drawn ROIs
            numROIs = height(labelsToAutomate);
            assert(numROIs >= 4, ...
                'You must create at least 4 LinePoints(2 lines), each in a different key frame.');
            
            
            % Check-2: Each frame must contain at most 1 ROI
            frameTime = labelsToAutomate.Time;
            disp(frameTime); 
            areTimestampsUnique = isequal(length(frameTime), length(unique(frameTime))*2);
            
            
            repeatedTime = unique(frameTime(diff(frameTime)==0));
            msg = sprintf('Expected one ROI per video frame. Remove extra ROIs from frames at time: %s\n', num2str(repeatedTime(:)'));
           
            assert(areTimestampsUnique, msg);
            
            
            % Check-3: All ROIs must belong to the same label.
            hasSameLabels = (length(unique(labelsToAutomate.Name))==1);
            assert(hasSameLabels, ...
                'Expected all ROIs to belong to the same label definition.');
            
            isReady = true;
        end
    end
    
    %----------------------------------------------------------------------
    % Execution
    %----------------------------------------------------------------------
    methods
        function initialize(algObj, ~, labelsToAutomate)
             
            % Store the initial labels
            algObj.InitialLabels = labelsToAutomate;

            % Store type and name
            algObj.ROIType = labelsToAutomate.Type{1};
            algObj.ROIName = labelsToAutomate.Name{1};
            

            % Compute sample time
            timeDuration = algObj.EndTime - algObj.StartTime;
            numFrames    = algObj.EndFrameIndex - algObj.StartFrameIndex;
            delT = timeDuration/numFrames;           
            
            % Compute interpolation parameters
            numROIs = height(labelsToAutomate);
 
            
            ROIPosition = labelsToAutomate.Position;
            ROITime = labelsToAutomate.Time;
            for n = 1:2: numROIs - 3
                     
                
                startROIpos1 = ROIPosition(n,:);
                startROIpos2 = ROIPosition(n+1,:);
                endROIpos1   = ROIPosition(n+2,:);
                endROIpos2 =  ROIPosition(n+3,:);
                
                numFramesForInterp = (ROITime(n+2) - ROITime(n))/delT;
                
                algObj.delX1(n) = (endROIpos1(1) - startROIpos1(1))/numFramesForInterp;
                algObj.delX2(n) = (endROIpos1(2) - startROIpos1(2))/numFramesForInterp;
                algObj.delY1(n) = (endROIpos2(1) - startROIpos2(1))/numFramesForInterp;
                algObj.delY2(n) = (endROIpos2(2) - startROIpos2(2))/numFramesForInterp;                      
            end
            
            % Initialize pointer index
            algObj.IsForwardAutomation = strcmpi(algObj.AutomationDirection, 'forward');
            if algObj.IsForwardAutomation
                algObj.Idx = 1;
                algObj.counter =1; 
            else
                algObj.Idx = size(algObj.InitialLabels,1);
            end
        end
        
        function autoLabels = run(algObj, ~)
            
            beforeFirstROIDrawTime  = (algObj.CurrentTime < algObj.InitialLabels.Time(1));
            afterLastROIDrawTime    = (algObj.CurrentTime > algObj.InitialLabels.Time(end));
            
            % If the current image frame is before the first drawn label or
            % after the last drawn label, there are no automated ROIs.
            if beforeFirstROIDrawTime || afterLastROIDrawTime
                
                autoLabels  = [];
                newLine1    = [];
                newLine2    = []; 
                
            % If the current frame has an ROI, record position of ROI and
            % increment index, there are no automated ROIs.
            elseif (algObj.CurrentTime == algObj.InitialLabels.Time(algObj.Idx)) %at ROI draw time
                
                newLine1 = algObj.InitialLabels.Position(algObj.Idx, :);
                newLine2 = algObj.InitialLabels.Position(algObj.Idx+1, :); 
                
                autoLabels = [];
                if algObj.IsForwardAutomation
                    algObj.Idx = algObj.Idx +  2;
                    algObj.counter = algObj.counter +1; 
                else
                    algObj.Idx = algObj.Idx -  2;
                    algObj.counter = algObj.counter -1; 
                end
            
            % Update the position of the ROI label and add an automated ROI
            % label
            else
                if algObj.IsForwardAutomation
                    idx = algObj.Idx-2;
                    
                    
                    newLine1 = algObj.prevLine1 + ...
                        [algObj.delX1(idx) algObj.delX2(idx)]; 
                    newLine2 = algObj.prevLine2 + ...
                       [algObj.delY1(idx) algObj.delY2(idx)]; 
                else
                    idx = algObj.Idx -2;
                    newLine1 = algObj.prevLine1 - ...
                        [algObj.delX1(idx) algObj.delX2(idx)] ; 
                    newLine2 = algObj.prevLine2 - ... 
                       [algObj.delY1(idx) algObj.delY2(idx)];                    
                end                
                
                autoLabels.Name     = algObj.ROIName;
                %autoLabels.Name (2)    = algObj.ROIName;
                autoLabels.Type     = algObj.ROIType;
                
                autoLabels.Position      = [newLine1;newLine2];
                
            end
                            
            % Update for next frame
            algObj.prevLine1 = newLine1;
            algObj.prevLine2 = newLine2; 
        end

        function terminate(algObj)
            
            % reset index           
            if algObj.IsForwardAutomation
                algObj.Idx = 1;
                algObj.counter =0; 
            else
                algObj.Idx = size(algObj.InitialLabels,1);
            end            
        end
    end
end