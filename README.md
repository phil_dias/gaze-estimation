# Gaze Estimation

Gaze Estimation for Assisted Living Environments. Accepted for WACV 2020. Preprint available at arXiv preprint https://arxiv.org/pdf/1909.09225.pdf.

Available under the Non-Profit Open Software License: for more details https://opensource.org/licenses/NPOSL-3.0

## Requirements:
- python3 and corresponding pip3
- OpenPose. Installation instructions: https://github.com/CMU-Perceptual-Computing-Lab/openpose/blob/master/doc/installation.md
- Keras, OpenCV (for reading/writing images) and some additional libraries all listed in requirements.txt
- (optional) virtualenv, which can be installed through 'pip install virtualenv' (see https://virtualenv.pypa.io/en/latest/installation/)

## Download, configuration and deploying the interface:
0. Install OpenPOse first
1. clone repository
2. cd gaze-estimation/
3. create virtual environment (optional): virtualenv . (if you have multiple python versions, run: virtualenv -p python3 .)
4. enter virtual environment (optional): source ./bin/activate
5. install requirements: pip3 install -r requirements.txt 
6. Configure path to OpenPose in gazeFromImages.py
7. run prediction on example images, using model trained on GazeFollow: python gazeFromImages.py
-- Images with overlaid gaze vectors and uncertainty estimations will be saved in ./outputs/gaze