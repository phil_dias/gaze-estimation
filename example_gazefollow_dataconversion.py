import os
import argparse
import cv2
import matplotlib.pyplot as plt
import numpy as np
import json

def load_poses_from_json(json_filename):

    with open(json_filename) as data_file:
        loaded = json.load(data_file)
        poses, conf = json_to_poses(loaded)

    if len(poses) != 1:
        return None, None
    else:
        return poses, conf

def load_many_poses_from_json(json_filename):

    with open(json_filename) as data_file:
        loaded = json.load(data_file)
        poses, conf = json_to_poses(loaded)

    return poses, conf

def json_to_poses(js_data): 

    poses = []
    confidences = []

    for arr in js_data["people"]:
        confidences.append(arr["pose_keypoints_2d"][2::3])
        arr = np.delete(arr["pose_keypoints_2d"], slice(2, None, 3))
        poses.append(list(zip(arr[::2],arr[1::2])))

    return poses, confidences


def compute_head_features(pose, conf):

    joints = [0,15,16,17,18]

    n_joints_set = [pose[joint] for joint in joints if joint_set(pose[joint])]

    if len(n_joints_set) < 1:
        return None, None

    centroid = compute_centroid(n_joints_set)

    max_dist = max([dist_2D(j, centroid) for j in n_joints_set])

    new_repr = [(np.array(pose[joint])-np.array(centroid)) for joint in joints]

    result = []

    for i in range(0, 5):

        if joint_set(pose[joints[i]]):
            result.append( [new_repr[i][0]/max_dist, new_repr[i][1]/max_dist] )
        else:
            result.append([0,0])

    flat_list = [item for sublist in result for item in sublist]

    for j in joints:
        flat_list.append(conf[j])

    return flat_list, centroid


def compute_centroid(points):

    mean_x = np.mean([p[0] for p in points])
    mean_y = np.mean([p[1] for p in points])

    return [mean_x, mean_y]

   
def joint_set(p):

    return p[0] != 0.0 or p[1] != 0.0


def dist_2D(p1, p2):
    p1 = np.array(p1)
    p2 = np.array(p2)

    squared_dist = np.sum((p1 - p2)**2, axis=0)
    return np.sqrt(squared_dist)


def select_pose(annotation, img, poses):

    centroids = []
    distances = []

    # - Annotation[6,7] corresponds to centroid of Ground Truth,
    # normalized with respect to image size
    annot_x = int(float(annotation[6]) * img.shape[1])
    annot_y = int(float(annotation[7]) * img.shape[0])

    # flag indicating if a valid detection (at least 2 keypts) was found
    validCentroid = False
    for i, pose in enumerate(poses):

        centroids.append(compute_head_centroid(poses[i]))

        if centroids[i] == None:
            
            distances.append(9999)
        
        else:
            validCentroid = True
            distances.append(dist_2D(centroids[i], [annot_x,annot_y]))

    # - adding my extra constraints: 
    # For each annotation centroid falls within 1.5x the distance between centroid and its farthest detected facial keypoint
    if validCentroid is True:
        # get info from centroid nearest to annotation 
        selPose = poses[distances.index(min(distances))]
        selCentroid = centroids[distances.index(min(distances))]

        joints = [0,15,16,17,18]

        n_joints_set = [selPose[joint] for joint in joints if joint_set(selPose[joint])]

        # distance between centroid and farthest detected keypoint
        max_dist = max([dist_2D(j, selCentroid) for j in n_joints_set])

        if annot_x >= selCentroid[0] - max_dist*1.5 \
            and annot_x <= selCentroid[0] + max_dist*1.5 \
            and annot_y >= selCentroid[1] - max_dist*1.5 \
            and annot_y <= selCentroid[1] + max_dist*1.5 :

            return distances.index(min(distances))

        else:
            return None
    else:
        return None            


def compute_head_centroid(pose):

    joints = [0,15,16,17,18]

    n_joints_set = [pose[joint] for joint in joints if joint_set(pose[joint])]

    if len(n_joints_set) < 2:
        return None

    centroid = compute_centroid(n_joints_set)

    return centroid

###################################################################

# defines input dataset and output file name

count = 0
data_set = 'train'

root_dir = "/home/philipe/Datasets/GazeFollow/data" 

# - For the gazefollow dataset here considered, GT annotations were available as text file
annotations = os.path.join(root_dir,'%s_annotations.txt' % data_set)
train_file = open(annotations, "r")

contents = train_file.readlines()
contents = [x.strip() for x in contents]
contents_split = [x.split(",") for x in contents]

output_file = os.path.join(root_dir, data_set, "featsGT_Full.npy")

ld = None
data = []

frames_ld = None
frames = []

for it in range(0, 119125):

	# - for the gazefollow dataset, input images folders & files had this pattern of names   
    top_level_dir = "%08d" % (int(it/1000))
    file_id_8digit = "%08d" % (it)

    json_dir = os.path.join(root_dir, 'json', data_set, top_level_dir)
    img_dir = os.path.join(root_dir, data_set, top_level_dir)

    json_filename = json_dir + '/' + file_id_8digit + '_keypoints.json'
    img_filename = img_dir + '/' + file_id_8digit + '.jpg'

    if os.path.exists(json_filename):

    	# loads from json outputs the many possible poses (peopel) detected in image
        poses, conf = load_many_poses_from_json(json_filename)

        # process them if the list is not empty
        if poses != None and poses != []:

            
            img = cv2.imread(img_filename)

            target_header = os.path.join(data_set, top_level_dir, file_id_8digit + '.jpg')
            
            if len(poses) == 1:
            	min_ind = 0
            
            else:
            	# -reduced contents here mean the entries in GT annotation text file;
            	# -in each line, first column has the file name, which is used here to find
            	# the annotations for the image being processed now;
            	# - each line is an annotated pose, so an image can have multiple entries (lines)
                contents_reduced = [line for line in contents_split if line[0] == target_header]

                
                annotation = contents_reduced[-1]
                # match the closest detected pose with annotation, based on annotated eye location
                min_ind = select_pose(annotation, img, poses)

            if min_ind is None:
                print("no pose selected:" + img_filename)
                continue

            # for the matched pose, compute centroid and normalized head features around this centroud
            features = compute_head_features(poses[min_ind], conf[min_ind])

            if features[0] is not None:

                contents_reduced = [line for line in contents_split if line[0] == target_header]

                if data_set == "test":

                    avg_versor = [0, 0]
                    count_avg = 0

                    # convert the annotations:
                    # 1) gazefollow annotations are given as percentages of Height and Width, so get abs. values first
                    # 2) ground truth gaze VERSORS used for network training are: 
                    # 		- centered w.r.t. to centroid; 
                    # 		- normalized w.r.t to farthest distance of a facial keypoint to the centroid 
                    for a in contents_reduced:

                    	# annotated eye coordinates
                        point1 = np.array(list(map(float,a[6:8])))
                        # annotated gaze vector endpoint
                        point2 = np.array(list(map(float,a[8:10])))

                        point1[0] *= img.shape[1]
                        point1[1] *= img.shape[0]
                        point2[0] *= img.shape[1]
                        point2[1] *= img.shape[0]

                        # get gaze versor
                        vector = point2 - point1
                        versor = vector / np.linalg.norm(vector)

                        avg_versor[0]  += versor[0]
                        avg_versor[1]  += versor[1]

                        count_avg += 1

                    # for test images, there were multiple annotations, so we would take avg here
                    avg_versor[0] /= count_avg
                    avg_versor[1] /= count_avg

                    data.append( features[0] + avg_versor )
                    ld = np.array(data)

                else:

                    for a in contents_reduced:

                        point1 = np.array(list(map(float,a[6:8])))
                        point2 = np.array(list(map(float,a[8:10])))

                        point1[0] *= img.shape[1]
                        point1[1] *= img.shape[0]
                        point2[0] *= img.shape[1]
                        point2[1] *= img.shape[0]

                        vector = point2 - point1
                        versor = vector / np.linalg.norm(vector)

                        # here is how the final feature matrix is arranged:
                        # ID (arbitrary just for indexing) | coordinates of features | confidence scores of detections | annotation versor
                        data.append([it] + features[1] + features[0] + list(versor) )
                        ld = np.array(data)

                        count += 1
            else:
                print("None features: "+img_filename)            
            if it%50 == 0:
                np.save(output_file,ld)
                print('\n### Done with: %d \n'%it)

            # output_file.write('%d\n' % it)
        else:
            print("None poses: "+img_filename)
    else:
        print("No JSON: "+img_filename)            

np.save(output_file,ld)
print('\n### FILE SAVED ###\n')