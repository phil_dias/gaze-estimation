import numpy as np
import json

import scipy.io as sio

def load_poses_from_json(json_filename):

    with open(json_filename) as data_file:
        loaded = json.load(data_file)
        poses, conf = json_to_poses(loaded)

    if len(poses) != 1:
        return None, None
    else:
        return poses, conf

def load_many_poses_from_json(json_filename):

    with open(json_filename) as data_file:
        loaded = json.load(data_file)
        poses, conf = json_to_poses(loaded)

    return poses, conf

def json_to_poses(js_data): 

    poses = []
    confidences = []

    for arr in js_data["people"]:
        confidences.append(arr["pose_keypoints_2d"][2::3])
        arr = np.delete(arr["pose_keypoints_2d"], slice(2, None, 3))
        poses.append(list(zip(arr[::2],arr[1::2])))


    return poses, confidences

def compute_head_features(pose, conf):

    joints = [0,15,16,17,18]

    n_joints_set = [pose[joint] for joint in joints if joint_set(pose[joint])]

    if len(n_joints_set) < 1:
        return None, None

    centroid = compute_centroid(n_joints_set)

    max_dist = max([dist_2D(j, centroid) for j in n_joints_set])

    new_repr = [(np.array(pose[joint])-np.array(centroid)) for joint in joints]

    result = []

    for i in range(0, 5):

        if joint_set(pose[joints[i]]):
            result.append( [new_repr[i][0]/max_dist, new_repr[i][1]/max_dist] )
        else:
            result.append([0,0])

    flat_list = [item for sublist in result for item in sublist]

    conf_list = []

    for j in joints:
        conf_list.append(conf[j])

    return flat_list, conf_list, centroid


def compute_body_features(pose, conf):

    joints = [0,15,16,17,18]
    alljoints = range(0,25)

    n_joints_set = [pose[joint] for joint in joints if joint_set(pose[joint])]

    if len(n_joints_set) < 1:
        return None, None

    centroid = compute_centroid(n_joints_set)

    n_joints_set = [pose[joint] for joint in alljoints if joint_set(pose[joint])]

    max_dist = max([dist_2D(j, centroid) for j in n_joints_set])

    new_repr = [(np.array(pose[joint])-np.array(centroid)) for joint in alljoints]

    result = []

    for i in range(0, 25):

        if joint_set(pose[i]):
            result.append( [new_repr[i][0]/max_dist, new_repr[i][1]/max_dist] )
        else:
            result.append([0,0])

    flat_list = [item for sublist in result for item in sublist]

    for j in alljoints:
        flat_list.append(conf[j])

    return flat_list, centroid


def compute_centroid(points):

    mean_x = np.mean([p[0] for p in points])
    mean_y = np.mean([p[1] for p in points])

    return [mean_x, mean_y]

   
def joint_set(p):

    return p[0] != 0.0 or p[1] != 0.0


def dist_2D(p1, p2):

    # print(p1)
    # print(p2)

    p1 = np.array(p1)
    p2 = np.array(p2)


    squared_dist = np.sum((p1 - p2)**2, axis=0)
    return np.sqrt(squared_dist)

def compute_head_centroid(pose):

    joints = [0,15,16,17,18]

    n_joints_set = [pose[joint] for joint in joints if joint_set(pose[joint])]

    if len(n_joints_set) < 2:
        return None

    centroid = compute_centroid(n_joints_set)

    return centroid