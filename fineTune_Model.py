##### !!!!!! NOTE: this code has not been cleaned/commented appropriately yet.
 # It has been uploaded only as an example of how to perform the fine-tuning described for the MoDiPro dataset.
 # Due to privacy issues, at the moment we are unable to share the MoDiPro dataset.

import numpy as np
import argparse
import random
import os
import csv
import cv2
import matplotlib.pyplot as plt
import json 
import math
import scipy.io as sio
import glob

from keras.models import Sequential, Model, load_model #Sequential Models
from keras.layers import Dense, Dropout, Conv1D, LocallyConnected1D, Flatten, Input, Reshape, Multiply, Lambda #Dense Fully Connected Layer Type
from keras.optimizers import SGD #Stochastic Gradient Descent Optimizer
from keras.optimizers import Adam 
from keras.utils import to_categorical
import keras.backend as K
import keras as keras
from keras import regularizers
from keras.callbacks import ModelCheckpoint
from sklearn.model_selection import train_test_split
#from mymodels import prepare_modelFineTune

# from mymodels import prepare_modelSingle
from finalmodels import prepare_modelSingle
# ---------------------------------------

def angular_error(y_true,y_pred):

	y_val = K.l2_normalize(y_true[:,:-1], axis=-1)
	y_predict = K.l2_normalize(y_pred[:,:-1], axis=-1)

	dot_ = K.batch_dot(y_val,y_predict,axes=1)
	results =  K.tf.acos(K.clip(dot_, -1, 1)) / np.pi * 180

	ang_error = K.mean(results)
	print(ang_error)

	return ang_error      


# MSE with uncertainty factor
def custom_loss(y_true,y_pred):

	MSE_ = K.mean(K.square(y_pred[:,:-1] - y_true[:,:-1]), axis=-1)
	loss = MSE_/(2*K.square(y_pred[:,-1])) + K.log(K.abs(y_pred[:,-1]))

	return loss

# Cosine loss with uncertainty factor
def custom_loss2(y_true,y_pred):

	var_pred = y_pred[:,-1]
	y_true = K.l2_normalize(y_true[:,:-1], axis=-1)
	y_pred = K.l2_normalize(y_pred[:,:-1], axis=-1)
	COS_ = (-K.sum(y_true * y_pred, axis=-1)+1)/2

	loss = COS_/(2*K.square(var_pred)) + K.log(K.abs(var_pred))

	return loss

# Cosine loss with uncertainty factor and using exp to avoid division by zero
# (used in final version of experiments)
def custom_lossLog(y_true,y_pred):

	var_pred = y_pred[:,-1]
	y_true = K.l2_normalize(y_true[:,:-1], axis=-1)
	y_pred = K.l2_normalize(y_pred[:,:-1], axis=-1)
	COS_ = (-K.sum(y_true * y_pred, axis=-1)+1)/2

	loss = (COS_*K.exp(-var_pred))/2 + (var_pred/2)

	# Return a function
	return loss

def my_init(shape, dtype=None):
	
	initVec = K.variable(np.ones(shape)*0.02)
	# initVec[:,:-1] = 
	# print(K.random_normal((shape[0],2)))
	K.set_value(initVec[:,:-1],K.eval(K.random_uniform((shape[0],2),minval=-0.1, maxval=0.1)))

	print(K.eval(initVec))
	return initVec

def unit_vector(vector):
	""" Returns the unit vector of the vector.  """
	return vector / np.linalg.norm(vector)

def angle_between(v1, v2):
	""" Returns the angle in radians between vectors 'v1' and 'v2'::

			>>> angle_between((1, 0, 0), (0, 1, 0))
			1.5707963267948966
			>>> angle_between((1, 0, 0), (1, 0, 0))
			0.0
			>>> angle_between((1, 0, 0), (-1, 0, 0))
			3.141592653589793
	"""
	v1_u = unit_vector(v1)
	v2_u = unit_vector(v2)
	return np.arccos(np.clip(np.dot(v1_u, v2_u), -1.0, 1.0))

# ---------------------------------------
# Function responsible for splitting the set of video sequences from a camera into 
# train/test according to the select split percentage
def my_train_test_split(X, y, seq, test_size, random_state):
	# get how many video sequences are in the set, and how many frames each has
	seqs, frPerSeq = np.unique(seq, return_counts=True)

	# total number of sequences
	noSeqs = len(seqs)

	# randomly shuffle the sequences by means of their IDs (0 to noSeqs)
	randSeqIds = np.arange(0,noSeqs) # IDs of random seeds
	random.shuffle(randSeqIds)
	

	# compute cumulative sum of total no. of frames when combining the sequences
	# according to randomly defined sequence
	listSeq = seqs[randSeqIds]
	cumSum_ = np.cumsum(frPerSeq[randSeqIds])
	
	# Starting from the 1st one in this randomly sorted sequence, calculate up to which sequence
	# should we pick to compute the train subset such that the amount of frames in the train/test sets
	# are the closest to the desired split percentage
	idMin = np.argmin(abs(cumSum_-test_size));

	# with this index in hand, just proceed and define the splits accordingly
	# seq_train, seq_test: list of video sequences composing each subset
	listSeq_test = listSeq[0:idMin+1]
	listSeq_train = listSeq[idMin+1:]

	# X_train, X_test: corresponding features (frames) composing each subset
	X_train = X[np.in1d(seq, listSeq_train).nonzero(),:][0]
	X_test = X[np.in1d(seq, listSeq_test).nonzero(),:][0]

	# Y_train, Y_test: corresponding labels (gaze Ground Truth) composing each subset
	y_train = y[np.in1d(seq, listSeq_train).nonzero(),:][0]
	y_test = y[np.in1d(seq, listSeq_test).nonzero(),:][0]

	seq_train =  seq[np.in1d(seq, listSeq_train).nonzero()]
	seq_test = seq[np.in1d(seq, listSeq_test).nonzero()]

	return X_train, X_test, y_train, y_test, seq_train, seq_test

# Stratified approach to divide dataset into Train/Val/Test.
# - We have multiple video sequences with varied amount of annotated frames. Since frames within same sequence
# tend to be strongly correlated (i.e. similar), we enforce that all frames from a video sequence belong to only one
# of the Train, Val or Test sets
# - To allow the different compositions described in Table 2 of the WACV paper, we split to video sequences of each 
# CAM1/CAM2 into Train/Val/Test and combine them accordingly for each experiment (e.g. train/test in single or both CAMs)
def prepare_train_test_val(setspath,X_cam1, X_cam2, y_cam1, y_cam2, seq_cam1, seq_cam2, split_perc, random_seed):

	# seqU_: list of video sequence IDs for each camera
	seqU_cam1 = np.unique(seq_cam1)
	seqU_cam2 = np.unique(seq_cam2)

	# full cameras
	seq_full1 = np.reshape(seqU_cam1,(seqU_cam1.shape[0],1))
	seq_full2 = np.reshape(seqU_cam2,(seqU_cam2.shape[0],1))	

	full_cam1 = 1*np.ones((seq_full1.shape))
	full_cam2 = 2*np.ones((seq_full2.shape))

	# split camera 1: we are defining 20% of the frames for test, 20% for val, 60% for training
	test_size = math.ceil(0.3*X_cam1.shape[0])
	val_size = math.ceil(0.2*X_cam1.shape[0])

	X_rest1, X_test1, y_rest1, y_test1, seq_rest1, seq_test1 = my_train_test_split(X_cam1, y_cam1, seq_cam1, test_size=test_size, random_state=random_seed)
	X_train1, X_val1, y_train1, y_val1, seq_train1, seq_val1 = my_train_test_split(X_rest1, y_rest1, seq_rest1, test_size=val_size, random_state=random_seed)

	seq_train1 = np.unique(seq_train1)
	seq_train1 = np.reshape(seq_train1,(seq_train1.shape[0],1))

	seq_test1 = np.unique(seq_test1)
	seq_test1 = np.reshape(seq_test1,(seq_test1.shape[0],1))

	seq_val1 = np.unique(seq_val1)
	seq_val1 = np.reshape(seq_val1,(seq_val1.shape[0],1))	

	print("Train: %d,%d; Val: %d,%d; Test: %d,%d ; Rest: %d,%d" %(X_train1.shape[0],X_train1.shape[1],\
													X_val1.shape[0],X_val1.shape[1],\
													X_test1.shape[0],X_test1.shape[1],\
													X_rest1.shape[0],X_rest1.shape[1]) )

	train_cam1 = 1*np.ones((seq_train1.shape))
	test_cam1 = 1*np.ones((seq_test1.shape))
	val_cam1 = 1*np.ones((seq_val1.shape))
	
	# split camera 2
	test_size = math.ceil(0.3*X_cam2.shape[0])
	val_size = math.ceil(0.2*X_cam2.shape[0])

	X_rest2, X_test2, y_rest2, y_test2, seq_rest2, seq_test2 = my_train_test_split(X_cam2, y_cam2, seq_cam2, test_size=test_size, random_state=random_seed)
	X_train2, X_val2, y_train2, y_val2, seq_train2, seq_val2 = my_train_test_split(X_rest2, y_rest2, seq_rest2, test_size=val_size, random_state=random_seed)

	seq_train2 = np.unique(seq_train2)
	seq_train2 = np.reshape(seq_train2,(seq_train2.shape[0],1))

	seq_test2 = np.unique(seq_test2)
	seq_test2 = np.reshape(seq_test2,(seq_test2.shape[0],1))

	seq_val2 = np.unique(seq_val2)
	seq_val2 = np.reshape(seq_val2,(seq_val2.shape[0],1))	

	print("Train: %d,%d; Val: %d,%d; Test: %d,%d ; Rest: %d,%d" %(X_train2.shape[0],X_train2.shape[1],\
													X_val2.shape[0],X_val2.shape[1],\
													X_test2.shape[0],X_test2.shape[1],\
													X_rest2.shape[0],X_rest2.shape[1]) )

	train_cam2 = 2*np.ones((seq_train2.shape))
	test_cam2 = 2*np.ones((seq_test2.shape))
	val_cam2 = 2*np.ones((seq_val2.shape))

	# For each experiment defined in Tab.2 of our WACV paper, combine the corresponding camera splits accordingly
	# to compose train/val/test
	for expid in np.arange(0,7):

		if expid % 7 == 0:

			X_train = X_train1
			y_train = y_train1
			
			X_test = X_test1
			y_test = y_test1

			X_val = X_val1
			y_val = y_val1

			train_cam = train_cam1
			seq_train = seq_train1 
			seq_train = np.concatenate((seq_train,train_cam),axis=1)

			test_cam = test_cam1
			seq_test = seq_test1
			seq_test = np.concatenate((seq_test,test_cam),axis=1)        

			val_cam = val_cam1
			seq_val = seq_val1
			seq_val = np.concatenate((seq_val,val_cam),axis=1)        

			# print(seq_test.shape)

		elif expid % 7 == 1:

			X_train = X_train1
			y_train = y_train1

			X_val = X_val1
			y_val = y_val1		
			
			X_test = X_test2
			y_test = y_test2

			train_cam = train_cam1
			seq_train = seq_train1 
			seq_train = np.concatenate((seq_train,train_cam),axis=1)

			val_cam = val_cam1
			seq_val = seq_val1
			seq_val = np.concatenate((seq_val,val_cam),axis=1)

			test_cam = test_cam2
			seq_test = seq_test2
			seq_test = np.concatenate((seq_test,test_cam),axis=1)       

		elif expid % 7 == 2:

			X_train = X_train2
			y_train = y_train2

			X_val = X_val2
			y_val = y_val2		
			
			X_test = X_test1
			y_test = y_test1

			train_cam = train_cam2
			seq_train = seq_train2 
			seq_train = np.concatenate((seq_train,train_cam),axis=1)

			val_cam = val_cam2
			seq_val = seq_val2
			seq_val = np.concatenate((seq_val,val_cam),axis=1)

			test_cam = test_cam1
			seq_test = seq_test1
			seq_test = np.concatenate((seq_test,test_cam),axis=1)       

		elif expid % 7 == 3:
	   
			X_train = X_train2
			y_train = y_train2

			X_val = X_val2
			y_val = y_val2		
			
			X_test = X_test2
			y_test = y_test2

			train_cam = train_cam2
			seq_train = seq_train2 
			seq_train = np.concatenate((seq_train,train_cam),axis=1)

			val_cam = val_cam2
			seq_val = seq_val2
			seq_val = np.concatenate((seq_val,val_cam),axis=1)

			test_cam = test_cam2
			seq_test = seq_test2
			seq_test = np.concatenate((seq_test,test_cam),axis=1)           
		  
		elif expid % 7 == 4:

			X_train = np.concatenate((X_train1, X_train2), axis=0)
			y_train = np.concatenate((y_train1, y_train2), axis=0)

			X_val = np.concatenate((X_val1, X_val2), axis=0)
			y_val = np.concatenate((y_val1, y_val2), axis=0) 		
			
			X_test = X_test1
			y_test = y_test1

			train_cam = np.concatenate((train_cam1,train_cam2),axis=0)
			seq_train = np.concatenate((seq_train1,seq_train2),axis=0)
			seq_train = np.concatenate((seq_train,train_cam),axis=1)

			val_cam = np.concatenate((val_cam1,val_cam2),axis=0)
			seq_val = np.concatenate((seq_val1,seq_val2),axis=0)			
			seq_val = np.concatenate((seq_val,val_cam),axis=1)

			test_cam = test_cam1
			seq_test = seq_test1
			seq_test = np.concatenate((seq_test,test_cam),axis=1)     

		elif expid % 7 == 5:

			X_train = np.concatenate((X_train1, X_train2), axis=0)
			y_train = np.concatenate((y_train1, y_train2), axis=0)

			X_val = np.concatenate((X_val1, X_val2), axis=0)
			y_val = np.concatenate((y_val1, y_val2), axis=0) 		
			
			X_test = X_test2
			y_test = y_test2

			train_cam = np.concatenate((train_cam1,train_cam2),axis=0)
			seq_train = np.concatenate((seq_train1,seq_train2),axis=0)
			seq_train = np.concatenate((seq_train,train_cam),axis=1)

			val_cam = np.concatenate((val_cam1,val_cam2),axis=0)
			seq_val = np.concatenate((seq_val1,seq_val2),axis=0)			
			seq_val = np.concatenate((seq_val,val_cam),axis=1)

			test_cam = test_cam2
			seq_test = seq_test2
			seq_test = np.concatenate((seq_test,test_cam),axis=1)     

		elif expid % 7 == 6:

			X_train = np.concatenate((X_train1, X_train2), axis=0)
			y_train = np.concatenate((y_train1, y_train2), axis=0)

			X_val = np.concatenate((X_val1, X_val2), axis=0)
			y_val = np.concatenate((y_val1, y_val2), axis=0) 		
			
			X_test = np.concatenate((X_test1, X_test2), axis=0)
			y_test = np.concatenate((y_test1, y_test2), axis=0)

			train_cam = np.concatenate((train_cam1,train_cam2),axis=0)
			seq_train = np.concatenate((seq_train1,seq_train2),axis=0)
			seq_train = np.concatenate((seq_train,train_cam),axis=1)

			val_cam = np.concatenate((val_cam1,val_cam2),axis=0)
			seq_val = np.concatenate((seq_val1,seq_val2),axis=0)			
			seq_val = np.concatenate((seq_val,val_cam),axis=1)

			test_cam = np.concatenate((test_cam1,test_cam2),axis=0)
			seq_test = np.concatenate((seq_test1,seq_test2),axis=0)			
			seq_test = np.concatenate((seq_test,test_cam),axis=1)  

		print("Train: %d,%d; Val: %d,%d; Test: %d,%d" %(X_train.shape[0],y_train.shape[0],\
														X_val.shape[0],y_val.shape[0],\
														X_test.shape[0],y_test.shape[0]) )

		setsFile = setspath + 'subSets%d.npz' % (expid)
		# saving train/test for comparison of multiple models
		np.savez(setsFile, X_train=X_train,X_test=X_test,X_val=X_val,\
						   y_train=y_train,y_test=y_test, y_val=y_val, \
						   seq_train=seq_train,seq_test=seq_test,seq_val=seq_val)

# Main function where model is declared, initialized and trained
def create_nn_model_exp(seed, tr_perc, load_data, modelid_par, save_par, loss_, base_path, numexp):

	random.seed(seed)

	# suffix defining where output files will be saved
	suffix_ = 'ArzFT_Split3b'

	# load features (dataset)
	data_arr = np.load(load_data)
	dataCam1 = data_arr['cam1_data']
	dataCam2 = data_arr['cam2_data']
	dataBoth = data_arr['both_data']

	seq_cam1 = dataCam1[:,1]
	seq_cam2 = dataCam2[:,1]

	X_cam1 = dataCam1[:,5:dataCam1.shape[1] - 2]
	y_cam1 = dataCam1[:,dataCam1.shape[1] - 2:]

	X_cam2 = dataCam2[:,5:dataCam2.shape[1] - 2]
	y_cam2 = dataCam2[:,dataCam2.shape[1] - 2:]

	# define optimizer 
	opt = 'adam'

	# no of epochs for training
	epochs = 1500

	# learning rate
	lr_val = 5e-5
	lrs = str(lr_val)

	# LR decay (we are not using any)
	decay_val = 0
	decay_str = str(decay_val)

	model_id = modelid_par

	# type of activation function for hidden layers
	activ = 'relu'

	if activ == 'relu':
		activ_func = 'relu'
	elif activ == 'tanh':
		activ_func = 'tanh' 

	if opt == 'sgd':

		optmiz = SGD(lr=lr_val, decay=decay_val, momentum=0.9, nesterov=True)

	elif opt == 'adam':
		optmiz = Adam(lr=lr_val, beta_1=0.9, beta_2=0.999, epsilon=1e-8, decay=decay_val, amsgrad=False)

	# call external function (library) where the model architecture is declared according to desired model_id
	model = prepare_modelSingle(model_id, activ_func)

	angArr = []

	class Metrics(keras.callbacks.Callback):

		def on_train_begin(self, logs={}):
			self._data = []
			self.losses = []
			self.val_losses = []

		# at the end of each epoch, compute validation error/loss
		def on_epoch_end(self, epoch, batch, logs={}):
			
			X_val, y_val = self.validation_data[0], self.validation_data[1]

			y_predict = np.asarray(model.predict(X_val))            

			results = [0] * y_val.shape[0]

			for i in range(0,y_val.shape[0]):

				results[i] = angle_between(y_val[i,:-1], y_predict[i,:-1]) / np.pi * 180

			self.losses.append(logs.get('loss'))
			self.val_losses.append(logs.get('val_loss'))          

			self._data.append({
				'ang_error': np.mean(results),
			})
			angArr.append(np.mean(results))

			# update plots every 50 epochs
			if (epoch+1)%50 == 0:
			   
				plt.figure(figsize=(6, 6))
				ax = plt.gca()
				plt.plot(angArr[:epoch], antialiased=True)
				plt.locator_params(axis='y', nbins=10)
				plt.title('Test Angular Error %.4f' % angArr[:epoch][-1])
				ax.set_xlim(-epoch/20, 1.05*epoch)
				plt.grid(True)

				model_str = 'model_%d_opt_%s_lr_%s_exp_%d_run_%d' % (model_id, opt, lrs, expid, run)

				plt.savefig(base_path+'plots%s/%s.png' % (suffix_,model_str))     
				plt.close()

			return

		def get_data(self):
			return [d.get('ang_error') for d in self._data]

		# defining function that computes angular error to compute it also for validation set during training
		def angular_error(y_true,y_pred):
			y_val = K.l2_normalize(y_true[:,:-1], axis=-1)
			y_predict = K.l2_normalize(y_pred[:,:-1], axis=-1)					
			results = [0] * y_val.shape[0]

			for i in range(0,y_val.shape[0]):

				results[i] = angle_between(y_val[i,:], y_predict[i,:]) / np.pi * 180

			ang_error = np.mean(results)
			
			return ang_error   

	metr = Metrics()
	results_ = np.zeros((4,numexp))
	losses_ = np.zeros((4,numexp))

	# run all different experiments/combinations
	for expid in np.arange(0,numexp):
		rowTable = 0

		model = prepare_modelSingle(model_id, activ_func)
		angArr = []		
		hist = []

		# split is done only once, s.t. that all experiments use same splits to allow comparison of results 
		# across them (final splits used for results reported in the paper are available in the repository)
		check_path = base_path+'plots%s' % (suffix_)
		if not os.path.exists(check_path):
			os.makedirs(check_path)

		check_path = base_path+'models%s' % (suffix_)
		if not os.path.exists(check_path):
			os.makedirs(check_path)

		for run in np.arange(2,3):
			setspath = base_path + 'setsplits%d/' % run
			
			if not os.path.exists(setspath):
				os.makedirs(setspath)	
				prepare_train_test_val(setspath, X_cam1, X_cam2, y_cam1, y_cam2, seq_cam1, seq_cam2, 0.30, seed)
			
			setsFile = setspath + 'subSets%d.npz' % (expid)

			data_arr = np.load(setsFile)

			X_train = data_arr['X_train']
			X_test = data_arr['X_test']
			X_val = data_arr['X_val']

			y_train = data_arr['y_train']
			y_test = data_arr['y_test']
			y_val = data_arr['y_val']

			seq_train = data_arr['seq_train']
			seq_test = data_arr['seq_test']
			seq_val = data_arr['seq_val']

			# pad labels with dummy column to match no of output units to handle uncertainty prediction
			zerocol = np.zeros((y_train.shape[0],1))
			y_train = np.append(y_train,zerocol,axis=1)

			zerocol = np.zeros((y_test.shape[0],1))

			zerocol = np.zeros((y_val.shape[0],1))
			y_val = np.append(y_val,zerocol,axis=1)						

			X_train = np.expand_dims(X_train,axis=2)
			X_test = np.expand_dims(X_test,axis=2)
			X_val = np.expand_dims(X_val,axis=2)

			# ID of combination (train,val), avoiding training two different models 
			# when they share the same train/val and change just test
			comb_ = expid // 2
			if comb_ > 1:
				comb_ = 2

			model_path = base_path+'models%s/comb_%d/'%(suffix_,comb_) 

			if not os.path.exists(model_path):
				os.makedirs(model_path)
				# load weights of model pre-trained on GazeFollow
				model.load_weights('../modelsSingleCentF/check_model_2_opt_adam_lr_0.005_decay_0_activ_relu_00000708.h5')

				model.compile(loss=custom_lossLog, optimizer=optmiz, metrics=[angular_error])

				# define the checkpoint base path (file preffix)
				checkpt_path = model_path + 'check_model_%d_opt_%s_lr_%s_run_%s_{epoch:08d}.h5' % (model_id, opt, lrs, run)

				# save checkpoint whenever validation error is the lowest so far, evaluating over periods of every 2 epochs
				checkpoint = ModelCheckpoint(checkpt_path, monitor='val_angular_error', mode='min', save_best_only=True, save_weights_only=True, period=2)
				callbacks_list = [checkpoint]

				hist = model.fit(X_train, y_train, validation_data=(X_val,y_val), epochs=epochs, batch_size=64, callbacks=[metr,checkpoint])
			
				# update table of results
				results_[rowTable,expid] = metr.get_data()[-1]
				losses_[rowTable,expid] = hist.history['loss'][-1]

				rowTable = rowTable + 1
				# plots
				plt.figure(figsize=(12, 8))

				ax = plt.subplot(221)
				plt.plot(1+np.arange(epochs), hist.history['loss'], antialiased=True)
				plt.title('Training Loss %.4f' % hist.history['loss'][-1])
				ax.set_ylim(-1, 1)
				ax.set_xlim(-epochs/20, 1.05*epochs)
				plt.grid(True)

				ax = plt.subplot(223)
				plt.plot(1+np.arange(epochs), hist.history['val_loss'], antialiased=True)
				plt.title('Test Loss %.4f' % hist.history['val_loss'][-1])
				ax.set_ylim(-1, 1)
				ax.set_xlim(-epochs/20, 1.05*epochs)
				plt.grid(True)

				ax = plt.subplot(224)
				plt.plot(1+np.arange(epochs), metr.get_data(), antialiased=True)
				plt.locator_params(axis='y', nbins=10)
				plt.title('Test Angular Error %.4f' % metr.get_data()[-1])
				ax.set_xlim(-epochs/20, 1.05*epochs)
				plt.grid(True)

				plt.close()

			## evaluate test with optimal model, which will be the last checkpoint saved using early stop
			# based on validation error
			optmodel_str =  model_path + 'check_model_%d_opt_%s_lr_%s_run_%s_*' % (model_id, opt, lrs, run)
			listfiles = glob.glob(optmodel_str)
			latest_file = max(listfiles, key=os.path.getctime)			

			# load these optimal weights and compute performance on test set then
			model.load_weights(latest_file)
			pred_ = model.predict(X_test, batch_size=32, verbose=0)

			y_predict = np.asarray(pred_)            

			results = [0] * y_test.shape[0]

			for i in range(0,y_test.shape[0]):

				results[i] = angle_between(y_test[i,:], y_predict[i,:-1]) / np.pi * 180	

			print("\n\n\n\n###### Experiment %d:%f #######\n\n\n\n" % (expid,np.nanmean(results)))				
		
			model_str = 'model_%d_opt_%s_lr_%s_exp_%d_run_%d' % (model_id, opt, lrs, expid, run)

			sio.savemat(base_path+'plots%s/%s.mat' % (suffix_,model_str) ,mdict={'pred_':pred_,'results':results})		   

	mat_str = 'model_%d_opt_%s_lr_%s_decay_%s_loss_%s' % ( model_id, opt, lrs, decay_str, loss_)    
	sio.savemat(base_path+'plots%s/%s.mat' % (suffix_, mat_str),mdict={'results_':results_,'losses_':losses_});

	return model


# ---------------------------------------
# ADJUST PATHS ACCORDINGLY
# The expected feature matrix is arranged as follows.
# - Each row is a detection + annotation, with columns as follows:
# ID (arbitrary just for indexing) | adj. coordinates X,Y of features | confidence scores of detections | annotation versor
datapath = '/path/with/features_and_annotations.npz'
base_path = '/path/with/the/source/codes'
save_par = True
numexp = 7 # parameter used to define which combination of CAM1, CAM2, CAM1&CAM2 we wanted to run in this experiment
modelid = 2 # 2: id of final optimal model
loss_ = 'sig'

clf = create_nn_model_exp(555, 1, datapath, modelid, save_par, loss_, base_path, numexp)