import os
import glob

import keras.backend as K

import numpy as np
import scipy.io as sio

import cv2
import matplotlib.pyplot as plt
# file containing NN architecture
from finalmodel import prepare_modelSingle
# file containing auxiliary functions that:
# - load poses detected from OpenPose saved as JSON
# - normalize/scale the detected facial features 
from featuresFun import load_many_poses_from_json, compute_head_features

# draw arrow illustrating gaze direction on the image
def draw_on_img(img, centr, id_, res):
	res[0] *= img.shape[0]
	res[1] *= img.shape[1]

	norm1 = res / np.linalg.norm(res)
	norm1[0] *= img.shape[0]*0.15
	norm1[1] *= img.shape[0]*0.15


	point = centr + norm1 

	# result = cv2.circle(img, (int(point[0]),int(point[1])), 5, (0,0,255), 2)
	result = cv2.arrowedLine(img, (int(centr[0]),int(centr[1])), (int(point[0]),int(point[1])), (0,0,0), thickness=3, tipLength=0.2)
	result = cv2.arrowedLine(result, (int(centr[0]),int(centr[1])), (int(point[0]),int(point[1])), (0,0,255), thickness=2, tipLength=0.2)

	txtPos = [int(centr[0]),int(centr[1]) -10]
	result = cv2.putText(result, str(id_), tuple(txtPos), cv2.FONT_HERSHEY_SIMPLEX ,  
	                   1, (0, 0, 255), 2, cv2.LINE_AA) 
	return result

# file containing weights of trained model
model_str = 'trainedOnGazeFollow_weights'

# input folder (needs absolute path)
thisPath = os.getcwd()
outputs_dir = os.path.join(thisPath,"outputs")
images_dir = os.path.join(thisPath,"images")

# enter path where OpenPose was installed
os.chdir('/usr/local/Frameworks/openpose')

# path for openpose deployment command
openpose_path = './build/examples/openpose/openpose.bin'

# define output paths for:
# - .json with OpenPose output
# - gaze overlaid in image
# - gaze + pose overlaid in image
out_json_dir = os.path.join(outputs_dir, 'json')
out_rend_dir = os.path.join(outputs_dir, 'rendered')
out_gaze_dir = os.path.join(outputs_dir, 'gaze')

if not os.path.exists(out_gaze_dir):
	os.makedirs(out_json_dir)
	os.makedirs(out_rend_dir)
	os.makedirs(out_gaze_dir)

# adjust path name for proper cmd syntax
direc = images_dir

direc = direc.replace(" ","\ ")
out_rend_dir = out_rend_dir.replace(" ","\ ")
out_json_dir = out_json_dir.replace(" ","\ ")

# define command to deploy OpenPose on the frames and run it
cmd = "%s --image_dir %s --display 0 --scale_number 4 --scale_gap 0.25 --write_images %s --write_json %s --num_gpu 1 --num_gpu_start 0" \
	 % (openpose_path,direc, out_rend_dir, out_json_dir)
# os.system(cmd)

os.chdir(thisPath)

model = prepare_modelSingle('relu')
model.load_weights('%s.h5' % model_str)

# for each image:
# 1. compute features (keypoints) from OpenPose detections
# 2. deploy network for gaze estimation
out_json_dir = os.path.join(outputs_dir, 'json')
out_rend_dir = os.path.join(outputs_dir, 'rendered')
out_gaze_dir = os.path.join(outputs_dir, 'gaze')

for json_filename in glob.glob(os.path.join(out_json_dir,"*.json")):
	_, suffix_  = os.path.split(json_filename)
	suffix_ = suffix_.split("_")
	suffix_ = suffix_[0]
	# collected poses from OpenPose detections
	poses, conf = load_many_poses_from_json(json_filename)

	data = []
	# for each pose
	for itP in range(0,len(poses)):
		try:
			# compute facial keypoints coordinates w.r.t. to head centroid
			features, confs, centr = compute_head_features(poses[itP], conf[itP])
			# if minimal amount of facial keypoints was detected
			if features is not None:   
				featMap = np.asarray(features)
				confMap = np.asarray(confs)

				featMap = np.reshape(featMap,(1,10))
				confMap = np.reshape(confMap,(1,5))

				centr = np.asarray(centr)
				centr = np.reshape(centr,(1,2))			

				poseFeats = np.concatenate((centr, featMap, confMap), axis=1)

				data.append(poseFeats)
		except Exception as e:
			print(e)
	
	# display on image
	imgfile = glob.glob(os.path.join(images_dir,suffix_+'*'))
	# this print is just to keep track which image/frame is currently being processed
	print(imgfile)
	img = cv2.imread(imgfile[0])
	imgRend = cv2.imread(os.path.join(out_rend_dir,suffix_ + '_rendered.png'))

	imgGaze = img
	rendGaze = imgRend		

	# if at least one valid pose (person) was detected, display his/her gaze direction on image
	if data:
		# adjust features to feed our gaze NN
		ld = np.array(data)	
		ld = np.squeeze(ld,axis=1)

		X_ = np.expand_dims(ld[:, 2:],axis=2)

		# deploy our gaze NN on these features
		# - prediction output: (X,Y,log of uncertainty)
		# -- (X,Y) are coordinates relative to centroid
		pred_ = model.predict(X_, batch_size=32, verbose=0)
	
		# uncomment following line if you want to save .mat files containing features/direction of gaze
		# gazeDirections = pred_[:,:-1]
		# Uncertainties = np.exp(pred_[:,-1])
		# Centroids = ld[:,0:2]
		# sio.savemat(outputs_dir+'/pred_%s.mat'%(suffix_),mdict={'gazeDirections':gazeDirections,'Uncertainties':Uncertainties,'Centroids':Centroids})

		uncStr = 'Uncertainties: '
		for itP in range(0,pred_.shape[0]):
			try:
				imgGaze = draw_on_img(imgGaze, ld[itP,0:2], itP, pred_[itP,:-1])
				rendGaze = draw_on_img(rendGaze, ld[itP,0:2], itP, pred_[itP,:-1])
				uncStr = uncStr + '%d: %.3f; '%(itP,np.exp(pred_[itP,-1]))
			except:
				print("skipped %d" %itP)

	# print uncertainty of each prediction on bottom of image
	txtPos = [0,imgGaze.shape[0]-30]
	imgGaze = cv2.putText(imgGaze, uncStr, tuple(txtPos), cv2.FONT_HERSHEY_SIMPLEX ,  
	                   1, (0, 255, 0), 2, cv2.LINE_AA) 
	rendGaze = cv2.putText(rendGaze, uncStr, tuple(txtPos), cv2.FONT_HERSHEY_SIMPLEX ,  
	                   1, (0, 255, 0), 2, cv2.LINE_AA) 

	cv2.imwrite(os.path.join(out_gaze_dir,'gaze_' + suffix_ + '.png'),imgGaze)
	cv2.imwrite(os.path.join(out_gaze_dir,'gazeR_' + suffix_ + '.png'),rendGaze)