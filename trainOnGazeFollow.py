import numpy as np
import random
import os
import matplotlib.pyplot as plt
import math

from keras.optimizers import SGD #Stochastic Gradient Descent Optimizer
from keras.optimizers import Adam 
import keras.backend as K
import keras as keras
from keras.callbacks import ModelCheckpoint
from sklearn.model_selection import train_test_split

from finalmodel import prepare_modelSingle
# ---------------------------------------

def angular_error(y_true,y_pred):

    y_val = K.l2_normalize(y_true[:,:-1], axis=-1)
    y_predict = K.l2_normalize(y_pred[:,:-1], axis=-1)

    dot_ = K.batch_dot(y_val,y_predict,axes=1)
    results =  K.tf.acos(K.clip(dot_, -1, 1)) / np.pi * 180

    ang_error = K.mean(results)
    print(ang_error)

    return ang_error      

# Cosine loss with uncertainty factor and using exp to avoid division by zero
def custom_lossLog(y_true,y_pred):

    var_pred = y_pred[:,-1]
    y_true = K.l2_normalize(y_true[:,:-1], axis=-1)
    y_pred = K.l2_normalize(y_pred[:,:-1], axis=-1)
    COS_ = (-K.sum(y_true * y_pred, axis=-1)+1)/2

    loss = (COS_*K.exp(-var_pred))/2 + (var_pred/2)

    return loss

def unit_vector(vector):
    """ Returns the unit vector of the vector.  """
    return vector / np.linalg.norm(vector)

def angle_between(v1, v2):
    """ Returns the angle in radians between vectors 'v1' and 'v2'::

            >>> angle_between((1, 0, 0), (0, 1, 0))
            1.5707963267948966
            >>> angle_between((1, 0, 0), (1, 0, 0))
            0.0
            >>> angle_between((1, 0, 0), (-1, 0, 0))
            3.141592653589793
    """
    v1_u = unit_vector(v1)
    v2_u = unit_vector(v2)
    return np.arccos(np.clip(np.dot(v1_u, v2_u), -1.0, 1.0))

# ---------------------------------------

def create_nn_model_exp(seed, tr_perc, load_data, base_path):

    # suffix defining where output files will be saved
    suffix_ = 'example'

    random.seed(seed)

    # load features (dataset)    
    data_arr = np.load(load_data)

    data_arr = [row for row in data_arr if not any(list(map(math.isnan,row)))]
    data_arr = np.array(data_arr)

    X_ = data_arr[:, 3:data_arr.shape[1] - 2]
    y_ = data_arr[:, data_arr.shape[1] - 2:]

    checkpoint_epochs = [50,100,150,200,250,300,600,1000]

    # define optimizer 
    opt = 'adam'
    optmiz = Adam(lr=lr_val, beta_1=0.9, beta_2=0.999, epsilon=1e-8, decay=decay_val, amsgrad=False)  

    # learning rate
    lr_val = 0.005
    lrs = str(lr_val)

    # no of epochs for training
    epochs = 1000

    # LR decay (we are not using any)
    decay_val = 0
    decay_str = str(decay_val)

    # type of activation function for hidden layers
    activ = 'relu'
    activ_func = 'relu'

    check_path = base_path+'models'+suffix_ 
    if not os.path.exists(check_path):
        os.makedirs(check_path)

    check_path = base_path+'plots'+suffix_
    if not os.path.exists(check_path):
        os.makedirs(check_path)

    # split into train, validation, test sets if file does not already exist
    setsFile = base_path+'setsCentr.npz' 

    if os.path.isfile(setsFile):
        data_arr = np.load(setsFile)

        X_train = data_arr['X_train']
        X_test = data_arr['X_test']
        y_train = data_arr['y_train']
        y_test = data_arr['y_test']

    else:

        X_train, X_test, y_train, y_test = train_test_split(X_, y_, test_size=0.10, random_state=seed)

        # pad labels with dummy column to match no of output units to handle uncertainty prediction
        zerocol = np.zeros((y_train.shape[0],1))
        y_train = np.append(y_train,zerocol,axis=1)

        zerocol = np.zeros((y_test.shape[0],1))
        y_test = np.append(y_test,zerocol,axis=1)

        # saving train/test for comparison of multiple models
        np.savez(setsFile, X_train=X_train,X_test=X_test,y_train=y_train,y_test=y_test)

    X_train = np.expand_dims(X_train,axis=2)
    X_test = np.expand_dims(X_test,axis=2)

    # call external function (library) where the model architecture is declared
    model = prepare_modelSingle(activ_func)

    # model.compile(loss=custom_lossLog, optimizer=optmiz)
    
    # variable to store angular error computed at each evaluation interval
    angArr = []

    class Metrics(keras.callbacks.Callback):

        def on_train_begin(self, logs={}):
            self._data = []
            self.losses = []
            self.val_losses = []
        
        # at the end of each epoch, compute validation error/loss
        def on_epoch_end(self, epoch, batch, logs={}):

            X_val, y_val = self.validation_data[0], self.validation_data[1]
            
            y_predict = np.asarray(model.predict(X_val))            

            results = [0] * y_val.shape[0]

            for i in range(0,y_val.shape[0]):

                results[i] = angle_between(y_val[i,:-1], y_predict[i,:-1]) / np.pi * 180

            self.losses.append(logs.get('loss'))
            self.val_losses.append(logs.get('val_loss'))          

            self._data.append({
                'ang_error': np.mean(results),
            })
            angArr.append(np.mean(results))

            # update plots every 50 epochs
            if (epoch+1)%50 == 0:
               
                plt.figure(figsize=(6, 6))
                ax = plt.gca()
                plt.plot(angArr[:epoch], antialiased=True)
                plt.locator_params(axis='y', nbins=10)
                plt.title('Test Angular Error %.4f' % angArr[:epoch][-1])
                ax.set_xlim(-epoch/20, 1.05*epoch)
                plt.grid(True)

                model_str = 'model_opt_%s_lr_%s_decay_%s_activ_%s' % (opt, lrs, decay_str, activ)

                plt.savefig(base_path+'plots%s/%s.png' % (suffix_,model_str))     

            return

        def get_data(self):
            return [d.get('ang_error') for d in self._data]

        def angular_error(y_true,y_pred):

            y_val = K.l2_normalize(y_true[:,:-1], axis=-1)
            y_predict = K.l2_normalize(y_pred[:,:-1], axis=-1)
                        
            results = [0] * y_val.shape[0]

            for i in range(0,y_val.shape[0]):

                results[i] = angle_between(y_val[i,:], y_predict[i,:]) / np.pi * 180

            ang_error = np.mean(results)

            return ang_error                        

    metr = Metrics()
    model.compile(loss=custom_lossLog, optimizer=optmiz, metrics=[angular_error])

    # define the checkpoint base path (file preffix)
    checkpt_path = 'models%s/check_model_opt_%s_lr_%s_decay_%s_activ_%s_{epoch:08d}.h5' % (suffix_, opt, lrs, decay_str, activ)

    # save checkpoint whenever validation error is the lowest so far, evaluating over periods of every 2 epochs
    checkpoint = ModelCheckpoint(checkpt_path, monitor='val_angular_error', mode='min', save_best_only=True, save_weights_only=True, period=2)
    callbacks_list = [checkpoint]

    hist = model.fit(x=X_train, y=y_train, validation_data=(X_test,y_test), epochs=epochs, batch_size=1024, callbacks=[metr,checkpoint])           
   
    # at the end, save loss/ang. error curves at each checkpoint epoch
    for (epoch) in checkpoint_epochs:
        # plots
        plt.figure(figsize=(12, 8))

        ax = plt.subplot(221)
        plt.plot(hist.history['loss'][:epoch], antialiased=True)
        plt.title('Training Loss %.4f' % hist.history['loss'][:epoch][-1])
        ax.set_ylim(-1, -0.25)
        ax.set_xlim(-epoch/20, 1.05*epoch)
        plt.grid(True)

        ax = plt.subplot(223)
        plt.plot(hist.history['val_loss'][:epoch], antialiased=True)
        plt.title('Test Loss %.4f' % hist.history['val_loss'][:epoch][-1])
        ax.set_ylim(-1, -0.25)
        ax.set_xlim(-epoch/20, 1.05*epoch)
        plt.grid(True)

        ax = plt.subplot(224)
        plt.plot(metr.get_data(), antialiased=True)
        plt.locator_params(axis='y', nbins=10)
        plt.title('Test Angular Error %.4f' % metr.get_data()[:epoch][-1])
        ax.set_xlim(-epoch/20, 1.05*epoch)
        plt.grid(True)

        model_str = 'keras_opt_%s_lr_%s_decay_%s_activ_%s_epochs_%d_gazefollow_all_samples' % (opt, lrs, decay_str, activ, epoch)

        plt.savefig(base_path+'plots%s/%s.png' % (suffix_,model_str))

    return model


# ---------------------------------------

# ADJUST PATHS ACCORDINGLY
# - path to file with input features (i.e. relative coordinates of the 5 facial keypoints, extract by OpenPose)
# - the example 'augCentrSingle_c' contain features already augmented as described in main manuscript
datapath = './data/augCentrSingle_c.npy'
base_path = '.'

clf = create_nn_model_exp(222, 1, datapath, base_path)